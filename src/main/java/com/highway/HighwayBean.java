package com.highway;

public class HighwayBean {
	
	private String highwayType;
	private String direction;
	private String quadrant;
	private String type;
	private int number;
	
	public HighwayBean(String highwayType) {
		this.highwayType = highwayType;
	}
	
	
	public HighwayBean() {
	}


	public String getHighwayType() {
		return highwayType;
	}
	public void setHighwayType(String highwayType) {
		this.highwayType = highwayType;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getQuadrant() {
		return quadrant;
	}
	public void setQuadrant(String quadrant) {
		this.quadrant = quadrant;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
	
	
	

}
