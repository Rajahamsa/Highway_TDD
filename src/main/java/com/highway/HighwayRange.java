package com.highway;

public class HighwayRange {

	HighwayBean hb=new HighwayBean();
	public HighwayBean checkRange(String highwayNumber) {
		String[] hNumber=highwayNumber.split("-");
		int number=Integer.parseInt(hNumber[1]);
		if(number<1||number>99) {
			throw new IllegalArgumentException("Number not in range");
		}
		hb.setType("hNumber[0]");
		hb.setNumber(number);
		return hb;
		
	}
	
	}


