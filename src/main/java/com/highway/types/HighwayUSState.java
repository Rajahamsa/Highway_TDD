package com.highway.types;

public class HighwayUSState implements HighwayInterface {

	public String getQuadrant(int highwayNumber) {
		String quadrant=null;
		if(highwayNumber%2==0) {
			if(highwayNumber>1&&highwayNumber<49) {
				quadrant="NORTH";
			}else if(highwayNumber>50&&highwayNumber<99) {
				quadrant="SOUTH";
			}
		}else {
			if(highwayNumber>=1&&highwayNumber<=49) {
				quadrant="EAST";
			}else if(highwayNumber>=50&&highwayNumber<=99) {
				quadrant="WEST";
			}
		}
		return quadrant;
	}


}
