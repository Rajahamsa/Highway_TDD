package com.highway.types;

public class HighwayInterstate implements HighwayInterface {

	public String getQuadrant(int highwayNumber) {
		String quadrant=null;
		if(highwayNumber%2==0) {
			if(highwayNumber>1&&highwayNumber<49) {
				quadrant="SOUTH";
			}else if(highwayNumber>50&&highwayNumber<99) {
				quadrant="NORTH";
			}
		}else {
			if(highwayNumber>=1&&highwayNumber<=49) {
				quadrant="WEST";
			}else if(highwayNumber>=50&&highwayNumber<=99) {
				quadrant="EAST";
			}
		}
		return quadrant;
	}

}
