package com.highway.types;

public interface HighwayInterface {

	String getQuadrant(int highwayNumber);

}
