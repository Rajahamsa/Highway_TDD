package com.highway;




import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class HighwayRangeTest {

	private HighwayRange h;
	private HighwayBean hb;
	@Before
	public void setUp() {
		h=new HighwayRange();
		hb=new HighwayBean();
	}

	@Rule
	public ExpectedException e=ExpectedException.none();
	
	@Test
	public void testAboveRange() {
		e.expect(IllegalArgumentException.class);
		e.expectMessage("Number not in range");
		h.checkRange("I-200");
		
	}
	
	@Test
	public void testBelowRange() {
		e.expect(IllegalArgumentException.class);
		e.expectMessage("Number not in range");
		h.checkRange("I-0");
		
	}
	
	@Test
	public void testInRange() {
		int expected=99;
		 hb=h.checkRange("I-99");
		 assertEquals(expected, hb.getNumber());
		
	}

}
