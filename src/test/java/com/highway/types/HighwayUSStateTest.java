package com.highway.types;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.highway.HighwayBean;
import com.highway.HighwayRange;

public class HighwayUSStateTest {
    private HighwayBean hb;
    private HighwayRange hr;
    private HighwayInterface hi;
    
	@Before
	public void setUp()  {
		hi = new HighwayUSState();
		hr = new HighwayRange();
		hb = new HighwayBean();
	}

	@Test
	public void testHighwayUSStateOddlessthan50() {
		hb=hr.checkRange("US-1");
		String expected="EAST";
		String actual=hi.getQuadrant(hb.getNumber());
		assertEquals(expected,actual);
	}
	
	@Test
	public void testHighwayUSStateOddgreaterthan50() {
		hb=hr.checkRange("US-51");
		String expected="WEST";
		String actual=hi.getQuadrant(hb.getNumber());
		assertEquals(expected,actual);
	}
	
	@Test
	public void testHighwayUSStateEvenlessthan50() {
		hb=hr.checkRange("US-34");
		String expected="NORTH";
		String actual=hi.getQuadrant(hb.getNumber());
		assertEquals(expected,actual);
	}
	
	@Test
	public void testHighwayUSStateEvenGreaterthan50() {
		hb=hr.checkRange("US-98");
		String expected="SOUTH";
		String actual=hi.getQuadrant(hb.getNumber());
		assertEquals(expected,actual);
	}

}
