package com.highway.types;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.highway.HighwayBean;
import com.highway.HighwayRange;

public class HighwayInterstateTest {

	private HighwayInterface hi;
	private HighwayBean b;
	private HighwayRange hr;

	@Before
	public void setUp() {
		hi = new HighwayInterstate();
		hr = new HighwayRange();
		b = new HighwayBean();
	}

	@Test
	public void testInterstateEvenNumberLessthan50() {
		b = hr.checkRange("I-32");
		String expected = "SOUTH";
		String actual = hi.getQuadrant(b.getNumber());
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInterstateEvenNumberGreaterthan50() {
		b = hr.checkRange("I-64");
		String expected = "NORTH";
		String actual = hi.getQuadrant(b.getNumber());
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInterstateOddNumberLessthan50() {
		b = hr.checkRange("I-31");
		String expected = "WEST";
		String actual = hi.getQuadrant(b.getNumber());
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInterstateOddNumberGreaterthan50() {
		b = hr.checkRange("I-69");
		String expected = "EAST";
		String actual = hi.getQuadrant(b.getNumber());
		assertEquals(expected, actual);
	}


}
